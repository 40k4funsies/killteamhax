##Instructions:##

***This is my workflow on Ubuntu linux using free open source software, printing on a Prusa Mk3 3D printer***

https://www.thingiverse.com/thing:3112142
https://gitlab.com/UltraSalem/killteamhax/tree/master/combatgauge3D

1. Download and install openSCAD (free open source software) - http://www.openscad.org/
2. Open .scad file in openSCAD
3. Use the in-built editor to change the "KIPP TEAT" text to whatever you might like
4. Press F6 to render
5. File -> Export -> STL
6. Download and install Slic3r (free open source software) - http://slic3r.org/
7. Open STL in Slic3r
8. **Important**: Scale to 2540% (this is the mm to inches conversion to make it the right size)
9. File -> Load config bundle. Choose the ini file from this project
10. Slice now
11. Save as gcode